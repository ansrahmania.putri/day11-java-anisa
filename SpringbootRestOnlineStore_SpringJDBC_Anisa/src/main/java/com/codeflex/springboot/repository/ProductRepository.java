package com.codeflex.springboot.repository;

import com.codeflex.springboot.model.Product;

import java.io.File;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface ProductRepository {

    Product findById(long id);

    List<Product> findByName(String name);

    int saveProduct(Product product);

    void saveVoid(Product product);

    int updateProduct(long id, Product product);

    int deleteProductById(long id);

    List<Product> findAllProducts();

    int[] batchInsert(List<Product> products);

    int[] batchUpdate(List<Product> products);

    int deleteAllProducts();

    boolean isProductExist(Product product);

    void save(Product product);
}
