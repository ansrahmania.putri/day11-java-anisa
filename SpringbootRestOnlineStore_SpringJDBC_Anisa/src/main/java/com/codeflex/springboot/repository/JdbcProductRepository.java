package com.codeflex.springboot.repository;

import com.codeflex.springboot.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.lob.LobHandler;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Repository

public class JdbcProductRepository implements ProductRepository{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public Product findById(long id) {
        return jdbcTemplate.queryForObject(
                "select * from product where id = ?",
                new Object[]{id},
                (rs, rowNum) ->
                        new Product(
                                rs.getLong("id"),
                                rs.getString("name"),
                                rs.getInt("categoryId"),
                                rs.getDouble("price")
                        )
        );
    }

    @Override
    public List<Product> findByName(String name) {
        return jdbcTemplate.query(
                "select * from product where name like ?",
                new Object[]{"%" + name + "%"},
                (rs, rowNum) ->
                        new Product(
                                rs.getLong("id"),
                                rs.getString("name"),
                                rs.getInt("categoryId"),
                                rs.getDouble("price")
                        )
        );

    }

    @Override
    public int saveProduct(Product product) {
        int ret = jdbcTemplate.update(
                "insert into product (name,categoryId,price) values (?,?,?)",
                product.getName(), product.getCategoryId(), product.getPrice());

        return ret;
    }

    @Override
    public void saveVoid(Product product) {
        jdbcTemplate.update(
                "insert into product (name,categoryId,price) values (?,?,?)",
                product.getName(), product.getCategoryId(), product.getPrice());
    }

    @Override
    public int updateProduct(long id, Product product) {
        return jdbcTemplate.update(
                "update product set name =?, categoryId =?, price=? where id =?",
                product.getName(), product.getCategoryId(), product.getPrice(), id);
    }

    @Override
    public int deleteProductById(long id) {
        return jdbcTemplate.update(
                "delete from product where id = ?",
                id);
    }

    @Override
    public List<Product> findAllProducts() {
        return jdbcTemplate.query(
                "select * from product",
                (rs, rowNum) ->
                        new Product(
                                rs.getLong("id"),
                                rs.getString("name"),
                                rs.getInt("categoryId"),
                                rs.getDouble("price")
                        )
        );
    }

    @Override
    public int[] batchInsert(List<Product> products) {
        return this.jdbcTemplate.batchUpdate(
                "insert into product (name,categoryId,price) values (?,?,?)",
                new BatchPreparedStatementSetter() {

                    public void setValues(PreparedStatement ps, int i) throws SQLException {
                        ps.setString(1, products.get(i).getName());
                        ps.setInt(2, products.get(i).getCategoryId());
                        ps.setDouble(3, products.get(i).getPrice());
                    }

                    public int getBatchSize() {
                        return products.size();
                    }

                });
    }

    @Override
    public int[] batchUpdate(List<Product> products) {
        return this.jdbcTemplate.batchUpdate(
                "update product set name =?, categoryId =?, price=? where id =?",
                new BatchPreparedStatementSetter() {

                    public void setValues(PreparedStatement ps, int i) throws SQLException {
                        ps.setString(1, products.get(i).getName());
                        ps.setInt(2, products.get(i).getCategoryId());
                        ps.setDouble(3, products.get(i).getPrice());
                        ps.setLong(4, products.get(i).getId());
                    }

                    public int getBatchSize() {
                        return products.size();
                    }

                });
    }

    @Override
    public int deleteAllProducts() {
        return jdbcTemplate.update(
                "delete from product");
    }

    @Override
    public boolean isProductExist(Product product) {
        return findByName(product.getName()) != null;
    }

    @Override
    public void save(Product product) {

    }


}
