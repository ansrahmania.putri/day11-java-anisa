package com.codeflex.springboot.service;

import com.codeflex.springboot.model.Product;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Service("ProductServiceDB")
public class ProductServiceDatabase implements ProductServiceDB{

    @Override
    public Product findById(long id) {
        Product product = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con= DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/sonoo","root","anisa2000");

            Statement stm = con.createStatement();
            String str = new String("SELECT * FROM product WHERE id = " + id);
            ResultSet rs=stm.executeQuery(str);

            while(rs.next()) {
                long idP = rs.getLong(1);
                String nameP = rs.getString(2);
                int categoryId = rs.getInt(3);
                Double price = rs.getDouble(4);

                product = new Product(idP, nameP, categoryId, price);
                product.setId(idP);
                product.setName(nameP);
                product.setCategoryId(categoryId);
                product.setPrice(price);
                System.out.println(product);
            }
            rs.close();
            stm.close();
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        return product;
    }

    @Override
    public Product findByName(String name) {
        Product product = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con= DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/sonoo","root","anisa2000");

            Statement stm = con.createStatement();
            String str = new String("SELECT * FROM product WHERE name LIKE = " + name);
            ResultSet rs=stm.executeQuery(str);

            while(rs.next()) {
                long idP = rs.getLong(1);
                String nameP = rs.getString(2);
                int categoryId = rs.getInt(3);
                Double price = rs.getDouble(4);

                product = new Product(idP, nameP, categoryId, price);
                product.setId(idP);
                product.setName(name);
                product.setCategoryId(categoryId);
                product.setPrice(price);
                System.out.println(product);
            }
            rs.close();
            stm.close();
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        return product;
    }

    @Override
    public void saveProduct(Product product) {

        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con= DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/sonoo","root","anisa2000");

            PreparedStatement prepStmt=con.prepareStatement("insert into product (name,categoryId,price) values (?,?,?)");
            prepStmt.setString(1,product.getName());//1 specifies the first parameter in the query
            prepStmt.setInt(2,product.getCategoryId());
            prepStmt.setDouble(3, product.getPrice());

            int i=prepStmt.executeUpdate();
            System.out.println(i+" records inserted");

            con.close();
        }catch(Exception e){ System.out.println(e);}

    }

    @Override
    public void updateProduct (long id, Product product) {

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con= DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/sonoo","root","anisa2000");

            PreparedStatement prepStmt=con.prepareStatement("UPDATE product SET name =?, categoryId =?, price=? WHERE id = " + id);
            prepStmt.setString(1,product.getName());
            prepStmt.setInt(2,product.getCategoryId());
            prepStmt.setDouble(3,product.getPrice());

            int j=prepStmt.executeUpdate();
            System.out.println("======"+ j +" records updated======");

            con.close();
        }catch(Exception e){ System.out.println(e);}

    }

    @Override
    public void deleteProductById(long id) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/sonoo", "root", "anisa2000");

            PreparedStatement prepStmt=con.prepareStatement("DELETE FROM product WHERE id =" + id);

            int j=prepStmt.executeUpdate();
            System.out.println("======"+ j +"  one product deleted======");

        } catch (Exception e) {
            System.out.println(e);
        }
    }

        @Override
    public List<Product> findAllProducts() {
       List<Product> prod = new ArrayList<>();
        try{

            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/sonoo", "root", "anisa2000");

            Statement stm = con.createStatement();
            String str = new String("SELECT * FROM product");
            ResultSet rs=stm.executeQuery(str);

            while(rs.next()) {
                long idP = rs.getLong(1);
                String nameP = rs.getString(2);
                int categoryId = rs.getInt(3);
                Double price = rs.getDouble(4);

                Product p = new Product(idP, nameP, categoryId, price);
                prod.add(p);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return prod;
    }

    @Override
    public void deleteAllProducts() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/sonoo", "root", "anisa2000");

            PreparedStatement prepStmt=con.prepareStatement("DELETE FROM product");

            int j=prepStmt.executeUpdate();
            System.out.println("======"+ j +" all product deleted======");

        } catch (Exception e) {
            System.out.println(e);
        }

    }

    @Override
    public boolean isProductExist(Product product) {
        return findByName(product.getName()) != null;
    }

}

