package com.electricalweb.application;

import com.electricalweb.daos.ProductDao;
import com.electricalweb.entities.Product;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class HibernateApplication {

    private final ProductDao productDao;
    private static BufferedReader productInputReader;

    public static void main(String[] args) throws Exception {
        EntityManager entityManager = Persistence
                .createEntityManagerFactory("product-unit")
                .createEntityManager();
        ProductDao productDao = new ProductDao(entityManager);
        BufferedReader productInputReader =
                new BufferedReader(new InputStreamReader(System.in));
        new HibernateApplication(productDao, productInputReader).run();
    }

    public HibernateApplication(ProductDao productDao, BufferedReader productInputReader) {
        this.productDao = productDao;
        this.productInputReader = productInputReader;
    }

    private void run() throws IOException {
        System.out.println("Enter an option: "
                + "1) Insert a new data. "
                + "2) Find a data. "
                + "3) Edit a data. "
                + "4) Delete a data.");
        int option = Integer.parseInt(productInputReader.readLine());

        switch (option) {
            case 1:
                persistNewProduct();
                break;
            case 2:
                fetchExistingProduct();
                break;
            case 3:
                updateExistingProduct();
                break;
            case 4:
                removeExistingProduct();
                break;
        }
    }

    private void persistNewProduct() throws IOException {
        long id = requestIntegerInput("the id of the product");
        String name = requestStringInput("the name of the product");
        int categoryId = requestIntegerInput("the categoryId of the product");
        double price = requestDoubleIntegerInput("the price of the product");
        productDao.persist(name, categoryId, price);
    }

    private void fetchExistingProduct() throws IOException {
        int id = requestIntegerInput("the product ID");
        Product product = productDao.find(id);
        System.out.print("Name : " + product.getName() + " CategoryId : " + product.getCategoryId() + " Price : " + product.getPrice() );
    }

    private void updateExistingProduct() throws IOException {
        int id = requestIntegerInput("the product ID");
        String name = requestStringInput("the name of the product");
        int categoryId = requestIntegerInput("the categoryId of the product");
        double price = requestDoubleIntegerInput("the price of the product");
        productDao.update(id, name, categoryId, price);
    }

    private void removeExistingProduct() throws IOException {
        int id = requestIntegerInput("the product ID");
        productDao.remove(id);
    }

    private String requestStringInput(String request) throws IOException {
        System.out.printf("Enter %s: ", request);
        return productInputReader.readLine();
    }

    private int requestIntegerInput(String request) throws IOException {
        System.out.printf("Enter %s: ", request);
        return Integer.parseInt(productInputReader.readLine());
    }

    private int requestDoubleIntegerInput(String request) throws IOException {
        System.out.printf("Enter %s: ", request);
        return (int) Double.parseDouble(productInputReader.readLine());
    }

}